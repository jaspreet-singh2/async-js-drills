const promise = require("promise");
const {  readFile, writeFile} = require("fs/promises");

const datafromnewfiles = [];

async function readfile2_1_2() {
  //Read the given file lipsum.txt
  // Convert the contents of the file lipsum.txt to uppercase & write the uppercase content to a new file called uppercase.txt. Store the name of the new file in filenames.txt');
  try {
    let fileData = await readFile("./problem2output/lipsum.txt", "utf8", "r");
    fileData = fileData.toUpperCase();
    //console.log(file)
    filename = "uppercase.txt";
    await writeFile(`./problem2output/${filename}`, fileData);
    await writeFile("./problem2output/filenames.txt", filename + "\n");
    return promise.resolve(filename);
  } catch (err) {
    console.log(err);
    return promise.reject();
  }
}

module.exports = readfile2_1_2