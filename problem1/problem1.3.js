const { writeFile, unlink } = require("fs/promises");

async function writeanddeleatefile() {
  try {
    await writeFile(`./problem1Output/file1.json`, JSON.stringify({ fileName: `file1` }));
    await writeFile(`./problem1Output/file2.json`, JSON.stringify({ fileName: `file2` }));
    await writeFile(`./problem1Output/file3.json`, JSON.stringify({ fileName: `file3` }));
    //console.log(`./file2.json is written `);

    unlink(`./problem1Output/file1.json`);
    unlink(`./problem1Output/file2.json`);
    unlink(`./problem1Output/file3.json`);

    //console.log(`./file${index}.json is deleted `);
  } catch (err) {
    console.log(err);
  }
}

writeanddeleatefile();
