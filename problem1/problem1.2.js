const { writeFile, unlink } = require("fs/promises");

function deletefile(i) {
  unlink(`./problem1Output/file${i}.json`)
    .then(() => {
      console.log(`file${i} is deleted`);
    })
    .catch((err) => console.log(err));
}


writeFile(`./problem1Output/file${1}.json`, JSON.stringify({ fileName: `file1` })).then(() => {
      
  writeFile(`./problem1Output/file${2}.json`, JSON.stringify({ fileName: `file2` })).then(() => {
          
    writeFile(`./problem1Output/file${3}.json`, JSON.stringify({ fileName: `file3` })).then(()=>
          {
            
            deletefile(1);
            deletefile(2);
            deletefile(3);
          });
    });
  }).catch((err) => console.log(err));

