
const promise = require("promise");
const { appendFile, readFile, writeFile  } = require("fs/promises");

const datafromnewfiles = [];

async function writetofile(index, data) {
    try {
      await writeFile(`./problem2output/sentence${index}.txt`, data);
      //console.log(`sentence${index}.txt is written `);
      await appendFile(`./problem2output/filenames.txt`, `sentence${index}.txt\n`);
      return promise.resolve(`writen to file index${index}`);
    } catch (err) {
      console.log(err);
    }
  }


async function toLowercase2_3(newFileName) {
    //Read the new file and convert it to lower case. Then split the contents into sentences. Then write each sentence into separate new files. Store the name of the new files in filenames.txt");
    try {
      let fileData = await readFile(`./problem2output/${newFileName}`, "utf8", "r");
      fileData = fileData.toLowerCase();
      splitParagraphs = fileData.split(/\n/);
      let sentences = [];
      for (let Paragraph of splitParagraphs) {
        if (Paragraph != "") {
          sentences = sentences.concat(Paragraph.split("."));
        }
      }
      writeFileP = [];
      for (let sentence in sentences) {
        if (sentences[sentence] != "") {
          writeFileP.push(writetofile(sentence, sentences[sentence]));
        }
      }
      return promise.all(writeFileP);
    } catch (err) {
  
      console.log(err);
      return promise.reject();
    }
  }

  module.exports = toLowercase2_3