//Convert the contents of the file lipsum.txt to uppercase & write the uppercase content to a new file called uppercase.txt. Store the name of the new file in filenames.txt

const readfile2_1_2 = require("./problem2_1_2");

const toLowercase2_3 = require("./problem2_3");

const readfile2_4 = require("./problem2_4");

const deletefileinfilesname2_5 = require("./problem2_5");

async function main() {
  //Read the given file lipsum.txt
  // Convert the contents of the file lipsum.txt to uppercase & write the uppercase content to a new file called uppercase.txt. Store the name of the new file in filenames.txt');
  let newFileName = await readfile2_1_2();
  //Read the new file and convert it to lower case. Then split the contents into sentences. Then write each sentence into separate new files. Store the name of the new files in filenames.txt");
  await toLowercase2_3(newFileName);
  //Read the new files, sort the content, write it out to a new file, called sorted.txt. Store the name of the new file in filenames.txt");
  await readfile2_4();
  //Read the contents of filenames.txt and delete all the new files that are mentioned in that list concurrently
  await deletefileinfilesname2_5();
  console.log('done')
}

main();
